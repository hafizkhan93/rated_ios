//
//  UniLoader.swift
//  RatEd_iOS
//
//  Created by Hafiz Khan on 14.01.19.
//  Copyright © 2019 Hafiz Khan. All rights reserved.
//

import Foundation
class UniversityLoader {
    var database: MockDatabase
    init(database: MockDatabase) {
        self.database = database
    }
    func loadObjects(with completion: ([University], UniversityViewModel) -> Void) {
        var uniEntries = [University]()
        self.database.loadObjects {(rawData) in
             let normalisedUniversityDataStream = rawData.compactMap({$0.value})
            for data in normalisedUniversityDataStream {
                guard let data = data as? NSDictionary else {
                    fatalError("Casting Error in Loader to a NSDictionary!")
                }
                let newUniversityEntry = University(uniData: data)
                uniEntries.append(newUniversityEntry)
            }
            let uniViewModel = UniversityViewModel(with: uniEntries)
            completion(uniEntries, uniViewModel)
        }
    }
}
