//
//  File.swift
//  RatEd_iOS
//
//  Created by Hafiz Khan on 14.01.19.
//  Copyright © 2019 Hafiz Khan. All rights reserved.
//

import Foundation
class University {
    var name: String?
    var location: String?
    var phoneNumber: String?
    var website: String?
    init(uniData: NSDictionary) {
        self.name = uniData["name"] as? String
        self.location = uniData["location"] as? String
        self.phoneNumber = uniData["phoneNr"] as? String
        self.website = uniData["website"]as? String
    }
}
