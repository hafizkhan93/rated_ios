//
//  Model.swift
//  RatEd_iOS
//
//  Created by Hafiz Khan on 16.01.19.
//  Copyright © 2019 Hafiz Khan. All rights reserved.
//

import Foundation

protocol Model {
    init()
}
