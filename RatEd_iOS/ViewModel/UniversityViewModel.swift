//
//  UniversityViewModel.swift
//  RatEd_iOS
//
//  Created by Hafiz Khan on 19.01.19.
//  Copyright © 2019 Hafiz Khan. All rights reserved.
//

//swiftlint:disable force_cast
//swiftlint:disable comma
import Foundation

enum UniversityDetailViewSectionType {
    case universityGeneralInfoSection
    case universityContactSection
}

protocol UniversityDetailViewSection {
    var type: UniversityDetailViewSectionType { get }
    var sectionTitle: String { get }
    var rowCount: Int { get }
    var universityID: String { get }
}

class UniversityGeneralInfoSection: UniversityDetailViewSection {
    var type: UniversityDetailViewSectionType {
        return .universityGeneralInfoSection
    }
    var sectionTitle: String {
        return "UniversityGeneralInfoSection"
    }
    var rowCount: Int {
        return 3
    }
    var universityID: String {
        return "someID"
    }
    var uniTitle: String
    var uniLocation: String
    var uniPhoneNr: String
    var uniWebAdress: String
    init(infoData: NSDictionary) {
        self.uniTitle = infoData["title"] as! String
        self.uniLocation = infoData["location"] as! String
        self.uniPhoneNr = infoData["phoneNr"] as! String
        self.uniWebAdress = infoData["web"] as! String
    }
}

class UniversityContactSection: UniversityDetailViewSection {
    var type: UniversityDetailViewSectionType {
        return .universityContactSection
    }
    var sectionTitle: String {
        return "UniversityContactSection"
    }
    var rowCount: Int {
        return 1
    }
    var universityID: String {
        return "Something"
    }
    var location: String?
    var phoneNr: String?
    var webAdress: String?
    init(data: NSDictionary) {
        self.location = data["location"] as? String
        self.phoneNr = data["phoneNr"] as? String
        self.webAdress = data["web"] as? String
    }
}

class UniversityViewModel: NSObject {
    public var items = [UniversityDetailViewSection]()
    override init() {
        super.init()
    }
    init(with data: [University]) {
        super.init()
        for uniEntry in data {
            if let title = uniEntry.name, let location = uniEntry.location,
                let phoneNr = uniEntry.phoneNumber, let web = uniEntry.website {
                let infoSectionData: [String: Any] =
                    ["title": title,"location": location,"phoneNr": phoneNr,"web": web]
                let newUniGeneralInfoSection =
                    UniversityGeneralInfoSection(infoData: infoSectionData as NSDictionary)
                let newUniContactSection = UniversityContactSection(data: infoSectionData as NSDictionary)
                items.append(newUniGeneralInfoSection)
                items.append(newUniContactSection)
            }
        }
    }
}
