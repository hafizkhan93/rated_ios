//
//  HomeViewControllerTableViewController.swift
//  RatEd_iOS
//
//  Created by Hafiz Khan on 02.10.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import UIKit

protocol ReceiveDataProtocol {
    func receivedData(data: inout UniversityViewModel, selectedUniOffset: Int)
}
//swiftlint:disable weak_delegate
class HomeViewController: UITableViewController {
    var homeViewData = [University]()
    var databaseReference: MockDatabase?
    var delegate: ReceiveDataProtocol?
    public var uniViewModel: UniversityViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        databaseReference = MockDatabase()
        requestLoader()
        tableView.register(UINib(nibName: "HomeViewCell", bundle: nil), forCellReuseIdentifier: "HomeViewCell")
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homeViewData.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = homeViewData[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomeViewCell") as? HomeViewCell
            else {
                fatalError("HomeViewCell could not be loaded!")
        }
        cell.item = item
        return cell
    }
    func requestLoader() {
        guard let databaseReference = databaseReference else {
            fatalError("Nil database!")
        }
        let homeViewLoader = UniversityLoader(database: databaseReference)
        homeViewLoader.loadObjects { (universityHomeData, uniViewModel) in
            self.uniViewModel = uniViewModel
            self.homeViewData = universityHomeData
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let uniViewController = UIStoryboard(name: "UniDetail", bundle: Bundle.main).instantiateInitialViewController()
            as? UniversityDetailViewController
        self.delegate = uniViewController
        if var viewModel = uniViewModel {
            delegate?.receivedData(data: &viewModel, selectedUniOffset: indexPath.row)
        } else {
            fatalError("UniViewModel not instantiated!") }
        self.navigationController?.pushViewController(uniViewController!, animated: true)
    }
}
