//
//  ProfileTableViewController.swift
//  RatEd_iOS
//
//  Created by Hafiz Khan on 14.11.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import UIKit

class ProfileTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Profile"

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }

}
