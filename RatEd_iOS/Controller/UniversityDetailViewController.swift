//
//  UniversityDetailViewController.swift
//  RatEd_iOS
//
//  Created by Hafiz Khan on 15.01.19.
//  Copyright © 2019 Hafiz Khan. All rights reserved.
//

import UIKit

class UniversityDetailViewController: UITableViewController, ReceiveDataProtocol {
    public var universityViewModel: UniversityViewModel?
    var selectedUniOffset: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "UniDetail"
        navigationController?.navigationBar.tintColor = UIColor.white
        self.tableView.register(
            UINib(nibName: "UniversityGeneralInfoCell", bundle: .main), forCellReuseIdentifier: "UniInfoCell")
        self.tableView.register(
            UINib(nibName: "UniversityContactCell", bundle: .main), forCellReuseIdentifier: "UniContactCell")
        self.tableView.reloadData()
    }
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func receivedData(data: inout UniversityViewModel, selectedUniOffset: Int) {
        self.universityViewModel = data
        self.selectedUniOffset = selectedUniOffset
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let items = self.universityViewModel?.items else {fatalError("no Array from ViewModel")}
        let item = items[indexPath.row + (2 * selectedUniOffset!)]
        switch item.type {
        case .universityGeneralInfoSection:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "UniInfoCell") as? UniversityGeneralInfoCell {
                cell.item = item
                return cell
            }
        case .universityContactSection:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "UniContactCell") as?
                UniversityContactCell {
                cell.item = item
                return cell
            }
        }
        return UITableViewCell()
    }
}
