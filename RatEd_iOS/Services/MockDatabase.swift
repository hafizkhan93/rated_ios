//
//  MockDatabase.swift
//  RatEd_iOS
//
//  Created by Hafiz Khan on 10.01.19.
//  Copyright © 2019 Hafiz Khan. All rights reserved.
//

import Foundation
// swiftlint:disable comma
class MockDatabase: Database {
    func loadObjects(with completion: ([String: Any]) -> Void) {
        let group = DispatchGroup()
        group.enter()
        var data = [String: Any]()
        DispatchQueue.global(qos: .default).async {
            _ = "Universities"
            let mockDataFetch =
["1": ["name": "TUWien","location": "Karlsplatz 13","phoneNr": "111","website": "www.test1.at"],
 "2": ["name": "WUWien","location": "Messe Prater ","phoneNr": "222","website": "www.test2.at"],
 "3": ["name": "UniWien","location": "Schottentor ","phoneNr": "333","website": "www.test3.at"]]
            data = mockDataFetch
            group.leave()
        }
        group.wait()
        completion(data)
    }
    func save(_ object: NSObject) {
        var test = ""
        test += ""
    }
}
