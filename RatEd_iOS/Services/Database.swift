//
//  File.swift
//  RatEd_iOS
//
//  Created by Hafiz Khan on 10.01.19.
//  Copyright © 2019 Hafiz Khan. All rights reserved.
//

import Foundation

protocol Database {
    func loadObjects(with completion: ([String: Any]) -> Void)
    func save(_ object: NSObject)
}
