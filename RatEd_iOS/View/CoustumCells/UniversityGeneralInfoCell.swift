//
//  UniversityGeneralInfoCell.swift
//  RatEd_iOS
//
//  Created by Hafiz Khan on 19.01.19.
//  Copyright © 2019 Hafiz Khan. All rights reserved.
//

import UIKit

class UniversityGeneralInfoCell: UITableViewCell {
    @IBOutlet weak var universityTitle: UILabel!
    @IBOutlet weak var universityLogo: UIImageView!
    var uniTitle: String?
    var uniLogo: String?
    var item: UniversityDetailViewSection? {
        didSet {
            guard let item = item as? UniversityGeneralInfoSection else { return }
            universityTitle?.text = item.uniTitle + "IT WORRRKS!!"
            let logoName = "14"
            universityLogo.image = UIImage(named: logoName)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
