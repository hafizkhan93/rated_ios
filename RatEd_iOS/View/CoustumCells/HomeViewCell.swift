//
//  HomeViewCell.swift
//  RatEd_iOS
//
//  Created by Hafiz Khan on 19.01.19.
//  Copyright © 2019 Hafiz Khan. All rights reserved.
//

import UIKit

class HomeViewCell: UITableViewCell {
    @IBOutlet weak var universityLabel: UILabel!
    var item: University? {
        didSet {
            universityLabel.text = item?.name
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
