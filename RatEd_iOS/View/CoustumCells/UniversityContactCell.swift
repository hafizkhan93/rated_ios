//
//  UniversityContactCell.swift
//  RatEd_iOS
//
//  Created by Hafiz Khan on 20.01.19.
//  Copyright © 2019 Hafiz Khan. All rights reserved.
//

import UIKit

class UniversityContactCell: UITableViewCell {
    @IBOutlet weak var webLabel: UILabel!
    @IBOutlet weak var phoneNrLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    var item: UniversityDetailViewSection? {
        didSet {
            guard let item = item as? UniversityContactSection else {return}
            locationLabel.text = item.location
            phoneNrLabel.text = item.phoneNr
            webLabel.text = item.webAdress
            }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
